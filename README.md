TrackMate-SpotOn
----------------
A simple TrackMate -> Spot-On connector

This connector allows to directly upload data tracked using TrackMate to SpotOn (https://spoton.berkeley.edu).

## Usage

1. Download the file `TrackMate_SpotOn-[VERSION].jar` and place it into the `plugins` folder of Fiji (can be downloaded from the [latest release](https://gitlab.com/tjian-darzacq-lab/Spot-On-TrackMate/tags/v1.1.0)).
2. Download the following three libraries:

   - json-simple
   - httpclient
   - httpcore
   
3. Place them into the `jars` folder of Fiji.
4. Open Fiji and TrackMate, and use TrackMate as you would with any tracking experiment
5. At the end of the wizard, in the "action" list, select "Upload to Spot-On" and click "Execute". The file should be sent to Spot-On, and the address should display in the log panel in the same window.

## Compilation
This script is written in Java and relies on `maven` to handle the dependencies.
This script comes with a simple Makefile. To compile and install:

1. Edit the file `Makefile` to point to the actual location of your Fiji installation.
3. Run `make && make install` to build the plugin


## It complains about "unable to find valid certification path to requested target"

This is a known limitation of Fiji, because it relies on an old version of Java, that does not incorporate yet the SSL certificates for Let's Encrypt.

A workaround is to proceed with the following commands:

Make sure that you replace `~/.Fiji.app/` by the actual path to your Fiji installation.

```{shell}
wget https://letsencrypt.org/certs/isrgrootx1.pem.txt
keytool -import -trustcacerts -alias letsencrypt -file isrgrootx1.pem.txt -keystore ~/.Fiji.app/java/linux-amd64/jdk1.8.0_66/jre/lib/security/cacerts
```

## Adding a custom Spot-On server

tbw.
