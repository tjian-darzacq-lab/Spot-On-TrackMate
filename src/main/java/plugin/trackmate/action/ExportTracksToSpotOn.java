package fiji.plugin.trackmate.action;

import fiji.plugin.trackmate.Logger;
import fiji.plugin.trackmate.Model;
import fiji.plugin.trackmate.Settings;
import fiji.plugin.trackmate.Spot;
import fiji.plugin.trackmate.TrackMate;
import fiji.plugin.trackmate.gui.TrackMateGUIController;
import fiji.plugin.trackmate.gui.TrackMateWizard;
import fiji.plugin.trackmate.io.IOUtils;
import fiji.plugin.trackmate.util.TMUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.ImageIcon;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.scijava.plugin.Plugin;

// HTTP POST & GET related imports
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.ClientProtocolException;
import java.io.UnsupportedEncodingException;
import java.lang.Float;
import org.apache.http.Consts;
import org.apache.http.entity.ContentType;
import org.apache.commons.codec.digest.DigestUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import javax.net.ssl.SSLContext;
import org.apache.http.ssl.SSLContexts;
import java.io.InputStream;

public class ExportTracksToSpotOn extends AbstractTMAction {

    public static final ImageIcon ICON = new ImageIcon(TrackMateWizard.class.getResource("images/spoton.png")); // Here play with the icon
	public static final String NAME = "Export tracks to Spot-On";

	public static final String KEY = "EXPORT_TRACKS_TO_SPOTON";
        /* TODO MW: TO REWRITE THIS */
	public static final String INFO_TEXT = "<html>" +
	    "Export the tracks in the current model content to Spot-On " +
	    "(https://spoton.berkeley.edu). " +
	    "<p> " +
	    "The tracks will be exported and then the address of the analysis " +
	    "will be displayed in the log window below. So far, only the main " +
	    "Spot-On instance is supported (https://spoton.berkeley.edu). " +
	    "Spatial units should be in microns and temporal units in ms</p> " +
	    "<p>" +
	    "As such, this format <u>cannot</u> handle track merging and " +
	    "splitting properly, and is suited only for non-branching tracks." +
	    "</html>";

	private final TrackMateGUIController controller;
    //private static String URL = "https://spoton.berkeley.edu/SPTGUI/uploadapi/"; // Spot-On URL, to be editable (TODO MW) MUST HAVE A TRAILING SLASH
    private static String URL = "https://spoton.berkeley.edu/SPTGUI/uploadapi/"; //https://spoton-beta.woringer.fr/SPTGUI/uploadapi/";
    
	/*
	 * CONSTRUCTOR
	 */

	public ExportTracksToSpotOn( final TrackMateGUIController controller )
	{
		this.controller = controller;
	}

	/*
	 * METHODS
	 */

	/**
	 * Static utility that silently exports tracks in a simplified xml format,
	 * describe in this class.
	 *
	 * @param model
	 *            the {@link Model} that contains the tracks to export.
	 * @param settings
	 *            a {@link Settings} object, only used to read its
	 *            {@link Settings#dt} field, the frame interval.
	 * @param file
	 *            the file to save to.
	 * @throws FileNotFoundException
	 *             if the target file cannot be written.
	 * @throws IOException
	 *             if there is a problem writing the file.
	 */
    public static void export(final Model model, final Settings settings, final File file) throws FileNotFoundException, IOException, UnsupportedEncodingException {
		final Element root = marshall(model, settings, Logger.VOID_LOGGER);
		final Document document = new Document(root);
		final XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		outputter.output(document, new FileOutputStream(file));
	}

	@Override
	public void execute(final TrackMate trackmate) {
	    // Convert the tracks to XML
	    // For now, this save the tracks to a file, but there is no reason to keep that in the future.
	    logger.log("Converting the tracks to simple XML format.\n");
	    final Model model = trackmate.getModel();
	    final int ntracks = model.getTrackModel().nTracks(true);
	    if (ntracks == 0) {
		logger.log("No visible track found. Aborting.\n");
		return;
	    }
	    
	    logger.log("  Preparing XML data.\n");
	    final Element root = marshall(model, trackmate.getSettings(), logger);
	    final Document document = new Document(root);

	    // =====
	    // ===== Processing to UPLOAD
	    // =====
	    logger.log("  Connecting to Spot-On:" + URL + "\n");

	    // ==== Convert and compute SHA1
	    String data = new XMLOutputter(Format.getPrettyFormat()).outputString(document); // Convert XML Document to String
	    String datasha = DigestUtils.sha1Hex(data); // Compute SHA1 (https://stackoverflow.com/questions/4400774/java-calculate-hex-representation-of-a-sha-1-digest-of-a-string)

	    // ==== Send GET with the SHA1 + where we want to save
	    // This takes a few lines because we want to use a custom SSL certificate
	    
	    // Trust own CA and all self-signed certs
	    SSLContext sslcontext = null;
	    try {
	    	sslcontext = SSLContexts.custom()
	    	    .loadTrustMaterial(getResourceAsFile("certs/keystore_letsencrypt"), "changeit".toCharArray(),
	    			       new TrustSelfSignedStrategy())
	    	    .build();
	    } catch (Exception e) {
	    	e.printStackTrace(); // TODO MW: handle this better
	    	return;
	    }
	    // Allow TLSv1 protocol only
	    SSLConnectionSocketFactory sslsf = 	new SSLConnectionSocketFactory(
	        sslcontext,
	    	new String[] { "TLSv1" },
	    	null,
	    	SSLConnectionSocketFactory.getDefaultHostnameVerifier());

	    CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();

		

	    // ==== End of tests
	    
	    //HttpClient httpclient = new DefaultHttpClient(); // Initialize connection

	    String dest = "new";
	    String getToken = null;
	    String getStatus = null;
	    String getMessage = null;

	    try {
		// The format of the request is documented into Spot-On.
		HttpGet httpget = new HttpGet(URL + "?sha=" + datasha + "&url=" + dest + "&format=trackmate&version=1.0");
		//logger.log("  Executing request " + httpget.getRequestLine() + "\n");

		// Create a custom response handler
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
			@Override
			public String handleResponse(
						     final HttpResponse response) throws ClientProtocolException, IOException {
			    int status = response.getStatusLine().getStatusCode();
			    if (status >= 200 && status < 300) {
				HttpEntity entity = response.getEntity();
				return entity != null ? EntityUtils.toString(entity) : null;
			    } else {
				throw new ClientProtocolException("Unexpected response status: " + status);
			    }
			}
			
		    };

		// Send the GET
		String responseBody = httpclient.execute(httpget, responseHandler);

		// Parse the answer
		JSONParser parser = new JSONParser();
		try {
		    Object obj = parser.parse(responseBody);
		    JSONObject jsonObject = (JSONObject) obj;
		    getStatus = (String) jsonObject.get("status");
		    getMessage = (String) jsonObject.get("message");
		    getToken = (String) jsonObject.get("token");

		    if (!getStatus.equals("success")) {
			logger.error("Could not establish connection with the Spot-On server\n");
			logger.error("The server said:\n");
			logger.error(getMessage+"\n");
			return;
		    } else {
			logger.log("  Connection established\n");
			//logger.log("DEBUG: token "+ getToken + "\n");
			
		    }
		} catch (Exception e) {
		    e.printStackTrace();
		}		
	    } catch (final IOException e) {
		logger.error("  It all failed miserably: "+ e.getMessage()+ "\n");
		return;
	    }
	    
	    
	    // Get ready to send the POST.
	    logger.log("  Uploading file.\n");
	    ProgressHttpEntityWrapper.ProgressListener listener = new ProgressHttpEntityWrapper.ProgressListener() {
		    // Callback for the progress of the upload
		    @Override public void transferred(long num, long num2) {
			String pr =  String.format("%.2f", ((float) num/num2)*100);
			logger.log("   Progress: "+ pr + "%\n");
		    }
		};
	    StringEntity se = new StringEntity(getToken+"||"+data, ContentType.create("text/plain", Consts.UTF_8));
	    ProgressHttpEntityWrapper ce = new ProgressHttpEntityWrapper(se, listener);
	    HttpPost httppost = new HttpPost(URL);
	    httppost.setEntity(ce);

	    //logger.log("DBG: request "+ httppost.getRequestLine() + "\n");
	    try {
	    	HttpResponse response = httpclient.execute(httppost);
	    	HttpEntity resEntity = response.getEntity();
		
	    	if (resEntity != null) {
	    	    //System.out.println(EntityUtils.toString(resEntity));
		    // Parse the answer
		    JSONParser parser = new JSONParser();
		    try {
			Object obj = parser.parse(EntityUtils.toString(resEntity));
			JSONObject jsonObject = (JSONObject) obj;
			getStatus = (String) jsonObject.get("status");
			getMessage = (String) jsonObject.get("message");
			getToken = (String) jsonObject.get("token");
			String getURL = (String) jsonObject.get("url");

			if (!getStatus.equals("success")) {
			    logger.error("Could not establish connection with the Spot-On server\n");
			    logger.error("The server said:\n");
			    logger.error(getMessage+"\n");
			    return;
			} else {
			    logger.log("File uploaded with success!\n");
			    logger.log("The file can now be viewed at:\n");
			    logger.log(getURL+"\n");
			}
		    } catch (Exception e) {
			e.printStackTrace();
			return;
		    }
	    	}
	    	if (resEntity != null) {
	    	    resEntity.consumeContent();
	    	}
		
	    	httpclient.getConnectionManager().shutdown();
	    } catch (final IOException e) {
	    	logger.error("ERROR: Error sending file:\n" + e.getMessage());
		return;
	    }
	}

	private static Element marshall(final Model model, final Settings settings, final Logger logger) {
		logger.setStatus("Marshalling...");
		final Element content = new Element(CONTENT_KEY);

		content.setAttribute(NTRACKS_ATT, ""+model.getTrackModel().nTracks(true));
		content.setAttribute(PHYSUNIT_ATT, model.getSpaceUnits());
		content.setAttribute(FRAMEINTERVAL_ATT, ""+settings.dt);
		content.setAttribute(FRAMEINTERVALUNIT_ATT, ""+model.getTimeUnits());
		content.setAttribute(DATE_ATT, TMUtils.getCurrentTimeString());
		content.setAttribute(FROM_ATT, TrackMate.PLUGIN_NAME_STR + " v" + TrackMate.PLUGIN_NAME_VERSION);

		final Set<Integer> trackIDs = model.getTrackModel().trackIDs(true);
		int i = 0;
		for (final Integer trackID : trackIDs) {

			final Set<Spot> track = model.getTrackModel().trackSpots(trackID);

			final Element trackElement = new Element(TRACK_KEY);
			trackElement.setAttribute(NSPOTS_ATT, ""+track.size());

			// Sort them by time
			final TreeSet<Spot> sortedTrack = new TreeSet<Spot>(Spot.timeComparator);
			sortedTrack.addAll(track);

			for (final Spot spot : sortedTrack) {
				final int frame = spot.getFeature(Spot.FRAME).intValue();
				final double x = spot.getFeature(Spot.POSITION_X);
				final double y = spot.getFeature(Spot.POSITION_Y);
				final double z = spot.getFeature(Spot.POSITION_Z);

				final Element spotElement = new Element(SPOT_KEY);
				spotElement.setAttribute(T_ATT, ""+frame);
				spotElement.setAttribute(X_ATT, ""+x);
				spotElement.setAttribute(Y_ATT, ""+y);
				spotElement.setAttribute(Z_ATT, ""+z);
				trackElement.addContent(spotElement);
			}
			content.addContent(trackElement);
			logger.setProgress(i++ / (0d + model.getTrackModel().nTracks(true)));
		}

		logger.setStatus("");
		logger.setProgress(1);
		return content;
	}

    private static File getResourceAsFile(String resourcePath) {
	// From: https://stackoverflow.com/a/35466006
	try {
	    //InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream(resourcePath);
	    InputStream in = TrackMateWizard.class.getResourceAsStream(resourcePath);
	    if (in == null) {
		return null;
	    }

	    File tempFile = File.createTempFile(String.valueOf(in.hashCode()), ".tmp");
	    tempFile.deleteOnExit();

	    try (FileOutputStream out = new FileOutputStream(tempFile)) {
		//copy stream
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = in.read(buffer)) != -1) {
		    out.write(buffer, 0, bytesRead);
		}
	    }
	    return tempFile;
	} catch (IOException e) {
	    e.printStackTrace();
	    return null;
	}
    }


	/*
	 * XML KEYS
	 */

	private static final String CONTENT_KEY 	= "Tracks";
	private static final String DATE_ATT 		= "generationDateTime";
	private static final String PHYSUNIT_ATT 	= "spaceUnits";
	private static final String FRAMEINTERVAL_ATT 	= "frameInterval";
	private static final String FRAMEINTERVALUNIT_ATT 	= "timeUnits";
	private static final String FROM_ATT 		= "from";
	private static final String NTRACKS_ATT		= "nTracks";
	private static final String NSPOTS_ATT		= "nSpots";


	private static final String TRACK_KEY = "particle";
	private static final String SPOT_KEY = "detection";
	private static final String X_ATT = "x";
	private static final String Y_ATT = "y";
	private static final String Z_ATT = "z";
	private static final String T_ATT = "t";


	@Plugin( type = TrackMateActionFactory.class )
	public static class Factory implements TrackMateActionFactory
	{

		@Override
		public String getInfoText()
		{
			return INFO_TEXT;
		}

		@Override
		public String getName()
		{
			return NAME;
		}

		@Override
		public String getKey()
		{
			return KEY;
		}

		@Override
		public TrackMateAction create( final TrackMateGUIController controller )
		{
			return new ExportTracksToSpotOn( controller );
		}

		@Override
		public ImageIcon getIcon()
		{
			return ICON;
		}
	}
}
