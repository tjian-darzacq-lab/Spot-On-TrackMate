all:
	mvn install

clean:
	rm target/*.jar
	rm ~/.Fiji.app/plugins/TrackMate_SpotOn*.jar
	rm ~/.Fiji.app/jars/json-simple*.jar
	rm ~/.Fiji.app/jars/httpcore-*.jar
	rm ~/.Fiji.app/jars/httpcore*.jar

install :
	mvn dependency:copy-dependencies
	cp target/dependency/json-simple-1.1.1.jar ~/.Fiji.app/jars/
	cp target/dependency/httpcore-*.jar ~/.Fiji.app/jars/
	cp target/dependency/httpclient-*.jar ~/.Fiji.app/jars/
	cp target/TrackMate_SpotOn-*.jar ~/.Fiji.app/plugins/

